"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const products_1 = require("../products");
function HomeComponent() {
    const [products, setProducts] = (0, react_1.useState)([]);
    (0, react_1.useEffect)(() => {
        (0, products_1.getProducts)().then(setProducts);
    }, []);
    return (react_1.default.createElement("div", { className: "my-10 grid grid-cols-4 gap-5" }, products.map((product) => (react_1.default.createElement("div", { key: product.id },
        react_1.default.createElement("img", { src: product.image, alt: product.name }),
        react_1.default.createElement("div", { className: "flex" },
            react_1.default.createElement("div", { className: "flex-grow font-bold" },
                react_1.default.createElement("a", null, product.name)),
            react_1.default.createElement("div", { className: "flex-end" }, products_1.currency.format(product.price))),
        react_1.default.createElement("div", { className: "text-sm mt-4" }, product.description))))));
}
exports.default = HomeComponent;
