"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const react_router_dom_1 = require("react-router-dom");
const products_1 = require("home/products");
function ProductDetail() {
    const [product, setProduct] = (0, react_1.useState)(null);
    const { id } = (0, react_router_dom_1.useParams)();
    (0, react_1.useEffect)(() => {
        id
            ? (0, products_1.getProductById)(id).then((product) => setProduct(product))
            : setProduct(null);
    }, [id]);
    if (!product)
        return null;
    return (react_1.default.createElement("div", { className: "grid grid-cols-2 gap-5" },
        react_1.default.createElement("div", null,
            react_1.default.createElement("img", { src: product.image, alt: product.name })),
        react_1.default.createElement("div", null,
            react_1.default.createElement("div", { className: "flex" },
                react_1.default.createElement("h1", { className: "font-bold text-3xl flex-grow" }, product.name),
                react_1.default.createElement("div", { className: "font-bold text-3xl flex-end" }, products_1.currency.format(product.price))),
            react_1.default.createElement("div", { className: "mt-10" }, product.description),
            react_1.default.createElement("div", { className: "mt-10" }, product.longDescription))));
}
exports.default = ProductDetail;
