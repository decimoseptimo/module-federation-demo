"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const react_dom_1 = __importDefault(require("react-dom"));
const react_router_dom_1 = require("react-router-dom");
require("./index.scss");
const Header_1 = __importDefault(require("home/Header"));
const Footer_1 = __importDefault(require("home/Footer"));
const ProductDetail_1 = __importDefault(require("./components/ProductDetail"));
const App = () => (react_1.default.createElement(react_router_dom_1.BrowserRouter, null,
    react_1.default.createElement("div", { className: "mt-10 text-3xl mx-auto max-w-6xl" },
        react_1.default.createElement(Header_1.default, null),
        react_1.default.createElement(react_router_dom_1.Routes, null,
            react_1.default.createElement(react_router_dom_1.Route, { path: "product/:id", element: react_1.default.createElement(ProductDetail_1.default, null) })),
        react_1.default.createElement(Footer_1.default, null))));
react_dom_1.default.render(react_1.default.createElement(App, null), document.getElementById("app"));
