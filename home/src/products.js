"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.currency = exports.getProductById = exports.getProducts = void 0;
const API_SERVER = "http://localhost:8080";
const getProducts = () => fetch(`${API_SERVER}/products`).then((res) => res.json());
exports.getProducts = getProducts;
const getProductById = (id) => fetch(`${API_SERVER}/products/${id}`).then((res) => res.json());
exports.getProductById = getProductById;
exports.currency = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
});
