"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
function Header() {
    return (react_1.default.createElement("div", { className: "p-5 bg-blue-500 text-white text-3xl font-bold" }, "Header V2"));
}
exports.default = Header;
